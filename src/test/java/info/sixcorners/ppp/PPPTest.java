package info.sixcorners.ppp;

import static lombok.AccessLevel.PRIVATE;
import static org.junit.Assert.assertEquals;

import java.math.BigInteger;

import lombok.val;
import lombok.experimental.FieldDefaults;

import org.junit.Test;

@FieldDefaults(level = PRIVATE, makeFinal = true)
public class PPPTest {
	PPPEncoder standard = new PPPEncoder(PPPEncoder.standardCharset);
	PPPEncoder extended = new PPPEncoder(PPPEncoder.extendedCharset);
	PPPGenerator generator = new PPPGenerator(
			"8651B6D5F361D3DA67E1D64C190B099DF71CA9B40D206D4D15AB83922B41CCD5");

	@FieldDefaults(level = PRIVATE, makeFinal = true)
	private enum PPPTestCase {
		t1(1, 1, 'A', 4, "uk%u", "+=uN"),
		t2(32, 5, 'C', 4, "b4Yw", "LU7S"),
		t3(PPPGenerator.MAX_COUNT.subtract(BigInteger.ONE), 16,
				"8:79v=im+9zTJbEv", "5.yt>t{>r~#E]ni}");
		PPPCoordinate coord;
		String standardResult, extendedResult;

		private PPPTestCase(long card, int row, char column, int passwordSize,
				String standardResult, String extendedResult) {
			this.coord = new PPPCoordinate(BigInteger.valueOf(card), row,
					column, passwordSize);
			this.standardResult = standardResult;
			this.extendedResult = extendedResult;
		}

		private PPPTestCase(BigInteger counter, int passwordSize,
				String standardResult, String extendedResult) {
			this.coord = PPPCoordinate.fromBigInteger(counter, passwordSize);
			this.standardResult = standardResult;
			this.extendedResult = extendedResult;
		}
	}

	@Test
	public void testPPP() {
		for (val result : PPPTestCase.values()) {
			val password = generator.generate(result.coord);
			assertEquals(result.toString(), result.standardResult,
					standard.encode(password, result.coord));
			assertEquals(result.toString(), result.extendedResult,
					extended.encode(password, result.coord));
		}
	}

	@FieldDefaults(level = PRIVATE, makeFinal = true)
	private enum CoordTestCase {
		t1(0, 1, 1, 'A', 4),
		t2(70, 2, 1, 'A', 4),
		t3(199995, 10000, 8, 'B', 16);
		BigInteger counter;
		PPPCoordinate coord;
		CoordTestCase(long counter, long card, int row, char column, int passwordSize) {
			this.counter = BigInteger.valueOf(counter);
			this.coord = new PPPCoordinate(BigInteger.valueOf(card), row, column, passwordSize);
		}
	}

	@Test
	public void testCoords() {
		for (val c : CoordTestCase.values()) {
			assertEquals(c.toString(), c.counter, c.coord.toBigInteger());
			val coord = PPPCoordinate.fromBigInteger(c.counter,
					c.coord.getPasswordSize());
			assertEquals(c.toString(), coord, c.coord);
		}
	}
}
