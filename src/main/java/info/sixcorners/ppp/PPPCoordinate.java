package info.sixcorners.ppp;

import java.math.BigInteger;

import lombok.Value;

/**
 * Helps with translating between a counter value and a set of card, row, and
 * column number values.
 */
@Value
public class PPPCoordinate {
	BigInteger card;
	int row;
	char column;
	int passwordSize;

	public BigInteger toBigInteger() {
		BigInteger card = this.card.subtract(BigInteger.ONE);
		int column = this.column - 'A';
		int row = this.row - 1;
		int columns = columnsFromPasswordSize(passwordSize);
		BigInteger fullCard = BigInteger.valueOf(10 * columns);

		BigInteger currentCard = BigInteger.valueOf(row * columns + column);
		return card.multiply(fullCard).add(currentCard);
	}

	public static PPPCoordinate fromBigInteger(BigInteger counter,
			int passwordSize) {
		BigInteger columns = BigInteger
				.valueOf(columnsFromPasswordSize(passwordSize));
		BigInteger fullCard = columns.multiply(BigInteger.TEN);
		BigInteger[] card = counter.divideAndRemainder(fullCard);
		BigInteger[] row = card[1].divideAndRemainder(columns);
		return new PPPCoordinate(card[0].add(BigInteger.ONE),
				row[0].intValue() + 1, (char) (row[1].intValue() + 'A'),
				passwordSize);
	}

	/**
	 * Determines how many columns a card should have. Every card has ten rows
	 * and a number of columns as determined by the size of each password.
	 *
	 * @param size
	 *            The size of a single password.
	 * @return The number of columns a card should have.
	 */
	public static int columnsFromPasswordSize(int size) {
		char last;
		switch (size) {
		case 2:
			last = 'K';
			break;
		case 3:
			last = 'H';
			break;
		case 4:
			last = 'G';
			break;
		case 5:
		case 6:
			last = 'E';
			break;
		case 7:
			last = 'D';
			break;
		case 8:
		case 9:
		case 10:
			last = 'C';
			break;
		case 11:
		case 12:
		case 13:
		case 14:
		case 15:
		case 16:
			last = 'B';
			break;
		default:
			throw new IllegalArgumentException();
		}
		return last - 'A' + 1;
	}
}
