package info.sixcorners.ppp;

import static javax.xml.bind.DatatypeConverter.parseHexBinary;

import java.math.BigInteger;
import java.security.Key;
import java.util.Arrays;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

import lombok.SneakyThrows;

/**
 * This class stores a key and generates passwords.
 *
 * @see #generate(BigInteger)
 */
public class PPPGenerator {
	/**
	 * 2^128. When the sequence starts to repeat.
	 */
	public static final BigInteger MAX_COUNT = BigInteger.ONE.shiftLeft(128);
	private final Cipher aes;

	/**
	 * Convenience constructor.
	 *
	 * @param key
	 *            The key in the form of a hex encoded string.
	 * @see #PPPGenerator(Key)
	 */
	public PPPGenerator(String key) {
		this(parseHexBinary(key));
	}

	/**
	 * Convenience constructor.
	 *
	 * @see #PPPGenerator(Key)
	 */
	public PPPGenerator(byte[] key) {
		this(new SecretKeySpec(key, "AES"));
	}

	/**
	 * Creates a password generator.
	 *
	 * @param key
	 *            The key to use to generate each password.
	 */
	@SneakyThrows
	public PPPGenerator(Key key) {
		aes = Cipher.getInstance("AES/ECB/NoPadding");
		aes.init(Cipher.ENCRYPT_MODE, key);
	}

	/**
	 * Convenience method.
	 *
	 * @see #generate(BigInteger)
	 */
	public BigInteger generate(PPPCoordinate coord) {
		return generate(coord.toBigInteger());
	}

	/**
	 * Generates a password.
	 *
	 * @param counter
	 *            Zero-based counter.
	 * @return An integer representing the password.
	 * @see PPPEncoder#encode(BigInteger, int)
	 */
	@SneakyThrows
	public BigInteger generate(BigInteger counter) {
		byte[] buf = counter.toByteArray();
		reverse(buf); // to little-endian
		buf = Arrays.copyOf(buf, 16); // extend/truncate
		aes.doFinal(buf, 0, buf.length, buf);
		reverse(buf); // to big-endian
		return new BigInteger(1, buf);
	}

	private static void reverse(byte[] a) {
		reverse(a, 0, a.length);
	}

	/**
	 * Reverses a in the range [i, j).
	 */
	private static void reverse(byte[] a, int i, int j) {
		for (j--; i < j; i++, j--) {
			byte t = a[i];
			a[i] = a[j];
			a[j] = t;
		}
	}
}
