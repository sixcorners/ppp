package info.sixcorners.ppp;

import java.math.BigInteger;
import java.util.Arrays;

import lombok.val;

/**
 * This class encodes passwords into strings.
 *
 * @see #encode(BigInteger, int)
 */
public class PPPEncoder {
	public static final String standardCharset = "!#%+23456789:=?@ABCDEFGHJKLMNPRSTUVWXYZabcdefghijkmnopqrstuvwxyz";
	public static final String extendedCharset = "!\"#$%&'()*+,-./23456789:;<=>?@ABCDEFGHJKLMNOPRSTUVWXYZ[\\]^_abcdefghijkmnopqrstuvwxyz{|}~";
	private final char[] charset;
	private final BigInteger charsetLen;

	/**
	 * Convenience constructor.
	 *
	 * @see #PPPEncoder(char[])
	 */
	public PPPEncoder(String charset) {
		this(charset.toCharArray());
	}

	/**
	 * Creates a new encoder with the specified character set.
	 *
	 * @param charset
	 *            An array of characters that will be used during encoding.
	 */
	public PPPEncoder(char[] charset) {
		this.charset = charset.clone();
		this.charsetLen = BigInteger.valueOf(this.charset.length);
		Arrays.sort(this.charset);
	}

	/**
	 * Convenience method.
	 *
	 * @see #encode(BigInteger, int)
	 */
	public String encode(BigInteger in, PPPCoordinate coord) {
		return encode(in, coord.getPasswordSize());
	}

	/**
	 * Encodes a password.
	 *
	 * @param in
	 *            An integer representing a password.
	 * @param size
	 *            The number of characters the password should have.
	 * @return A string representation of the password.
	 * @see PPPGenerator#generate(BigInteger)
	 */
	public String encode(BigInteger in, int size) {
		val ret = new StringBuilder(size);
		for (int i = 0; i < size; i++) {
			val dar = in.divideAndRemainder(charsetLen);
			in = dar[0];
			ret.append(charset[dar[1].intValue()]);
		}
		return ret.toString();
	}
}
